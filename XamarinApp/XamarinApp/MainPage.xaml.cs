﻿using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            var span = new Span()
            {
                Text = "Linkedin",
                TextColor = Color.FromHex("#1E90FF")
            };
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (s, e) => {
                // handle the tap
                Device.OpenUri(new System.Uri("https://www.linkedin.com/in/thomas-lambert-341b4814b/"));
            };
            span.GestureRecognizers.Add(tapGestureRecognizer);
            MainString.Spans.Add(span);
        }
    }
}
